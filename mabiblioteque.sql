-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 07 fév. 2020 à 07:32
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mabiblioteque`
--

-- --------------------------------------------------------

--
-- Structure de la table `adherents`
--

DROP TABLE IF EXISTS `adherents`;
CREATE TABLE IF NOT EXISTS `adherents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `adherents`
--

INSERT INTO `adherents` (`id`, `pseudo`) VALUES
(41, 'David'),
(42, 'Alexandria'),
(43, 'Maurice'),
(44, 'Jean'),
(45, 'Michel'),
(46, 'Margaux'),
(47, 'Adrienne'),
(48, 'Gilles');

-- --------------------------------------------------------

--
-- Structure de la table `antenne`
--

DROP TABLE IF EXISTS `antenne`;
CREATE TABLE IF NOT EXISTS `antenne` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `antenne`
--

INSERT INTO `antenne` (`id`, `ville`) VALUES
(41, 'Fleury'),
(42, 'Marchand'),
(43, 'Hernandez-sur-Dupre'),
(44, 'Bigotnec'),
(45, 'Klein-la-Forêt'),
(46, 'Lefebvre'),
(47, 'Le Goff'),
(48, 'Bertrand');

-- --------------------------------------------------------

--
-- Structure de la table `auteur`
--

DROP TABLE IF EXISTS `auteur`;
CREATE TABLE IF NOT EXISTS `auteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_naissance` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `auteur`
--

INSERT INTO `auteur` (`id`, `nom`, `prenom`, `date_naissance`) VALUES
(91, 'Vallet', 'Élisabeth', '2016-02-05'),
(92, 'Camus', 'Alfred', '2017-08-28'),
(93, 'Olivier', 'Margaux', '2015-01-04'),
(94, 'Labbe', 'Jules', '2017-01-13'),
(95, 'Techer', 'Marthe', '2011-07-25'),
(96, 'Daniel', 'Arthur', '2017-05-03'),
(97, 'Hubert', 'Christophe', '2013-04-04'),
(98, 'Denis', 'Jeannine', '2015-10-31'),
(99, 'Raymond', 'Daniel', '2013-01-15'),
(100, 'Blondel', 'Tristan', '2017-07-28'),
(101, 'bonaparte', 'Napoléon', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `exemplaire`
--

DROP TABLE IF EXISTS `exemplaire`;
CREATE TABLE IF NOT EXISTS `exemplaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `etat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posseder_id` int(11) DEFAULT NULL,
  `correspondre_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5EF83C921DB77787` (`posseder_id`),
  KEY `IDX_5EF83C92D793666D` (`correspondre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `exemplaire`
--

INSERT INTO `exemplaire` (`id`, `etat`, `posseder_id`, `correspondre_id`) VALUES
(25, 'neuf', 48, 232),
(26, 'neuf', 48, 232),
(27, 'neuf', 48, 232),
(28, 'neuf', 48, 232),
(29, 'neuf', 48, 232),
(30, 'neuf', 48, 232),
(31, 'neuf', 48, 232),
(32, 'neuf', 48, 232),
(33, 'mauvais', 44, 188),
(34, 'passable', 43, 225);

-- --------------------------------------------------------

--
-- Structure de la table `exemplaire_adherents`
--

DROP TABLE IF EXISTS `exemplaire_adherents`;
CREATE TABLE IF NOT EXISTS `exemplaire_adherents` (
  `exemplaire_id` int(11) NOT NULL,
  `adherents_id` int(11) NOT NULL,
  PRIMARY KEY (`exemplaire_id`,`adherents_id`),
  KEY `IDX_498F2AB95843AA21` (`exemplaire_id`),
  KEY `IDX_498F2AB915364D07` (`adherents_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `exemplaire_adherents`
--

INSERT INTO `exemplaire_adherents` (`exemplaire_id`, `adherents_id`) VALUES
(34, 43),
(34, 45);

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20200204171058', '2020-02-04 17:11:16'),
('20200204172437', '2020-02-04 17:25:02'),
('20200206115617', '2020-02-06 11:56:30');

-- --------------------------------------------------------

--
-- Structure de la table `mots_cles`
--

DROP TABLE IF EXISTS `mots_cles`;
CREATE TABLE IF NOT EXISTS `mots_cles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mots_cles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `mots_cles`
--

INSERT INTO `mots_cles` (`id`, `mots_cles`) VALUES
(106, 'voluptatibus'),
(107, 'aperiam'),
(108, 'iusto'),
(109, 'deleniti'),
(110, 'animi'),
(111, 'eaque'),
(112, 'et'),
(113, 'vel'),
(114, 'debitis'),
(115, 'aspernatur'),
(116, 'voluptates'),
(117, 'blanditiis'),
(118, 'fugit'),
(119, 'reprehenderit'),
(120, 'dolores'),
(121, 'hic'),
(122, 'animi'),
(123, 'distinctio'),
(124, 'voluptas'),
(125, 'recusandae');

-- --------------------------------------------------------

--
-- Structure de la table `ouvrage`
--

DROP TABLE IF EXISTS `ouvrage`;
CREATE TABLE IF NOT EXISTS `ouvrage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_parution` datetime NOT NULL,
  `ranger_id` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_52A8CBD8E381DB8A` (`ranger_id`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ouvrage`
--

INSERT INTO `ouvrage` (`id`, `titre`, `date_parution`, `ranger_id`, `image`) VALUES
(188, 'la belle et la bete', '2015-05-26 00:00:00', 36, 'http://placehold.it/200*350'),
(189, 'mon livre', '2020-10-02 00:00:00', 39, 'http://placehold.it/200*350'),
(190, 'non', '2015-12-16 08:47:27', 36, 'http://placehold.it/200*350'),
(191, 'voluptatem', '2017-07-23 11:03:48', 36, 'http://placehold.it/200*350'),
(192, 'exercitationem', '2019-02-21 23:27:52', 36, 'http://placehold.it/200*350'),
(193, 'eaque', '2017-04-08 09:41:41', 36, NULL),
(194, 'dolor', '2019-04-12 22:39:05', 36, NULL),
(195, 'sit', '2012-02-05 05:45:47', 36, NULL),
(196, 'rerum', '2010-04-02 15:07:51', 37, NULL),
(197, 'saepe', '2019-03-26 22:02:50', 37, NULL),
(198, 'et', '2012-05-10 17:50:07', 37, NULL),
(199, 'maiores', '2019-07-05 11:21:18', 37, NULL),
(200, 'magnam', '2014-10-08 00:20:09', 37, NULL),
(201, 'repellat', '2010-02-23 06:48:33', 37, NULL),
(202, 'tenetur', '2016-11-13 08:38:41', 37, NULL),
(203, 'in', '2013-07-07 22:31:04', 37, NULL),
(204, 'impedit', '2010-03-01 06:57:16', 38, NULL),
(205, 'ea', '2018-12-15 20:51:06', 38, NULL),
(206, 'mollitia', '2015-06-20 08:31:39', 38, NULL),
(207, 'rem', '2015-08-11 08:17:49', 38, NULL),
(208, 'natus', '2018-02-13 06:13:09', 38, NULL),
(209, 'qui', '2016-09-23 21:52:13', 38, NULL),
(210, 'animi', '2010-07-02 14:38:58', 39, NULL),
(211, 'repellendus', '2011-07-04 10:47:56', 39, NULL),
(212, 'reiciendis', '2010-05-12 11:07:42', 39, NULL),
(213, 'ab', '2014-12-26 20:47:13', 39, NULL),
(214, 'ut', '2012-04-10 13:14:22', 39, NULL),
(215, 'architecto', '2018-05-13 22:18:11', 39, NULL),
(216, 'dolores', '2015-09-14 11:13:03', 39, NULL),
(217, 'perspiciatis', '2016-02-21 11:14:40', 39, NULL),
(218, 'harum', '2017-03-12 10:31:31', 39, NULL),
(219, 'repudiandae', '2015-10-28 23:56:47', 39, NULL),
(220, 'voluptatem', '2014-05-31 13:38:32', 39, NULL),
(221, 'eius', '2013-10-31 06:57:17', 39, NULL),
(222, 'vel', '2017-07-04 02:25:21', 39, NULL),
(223, 'exercitationem', '2019-01-12 20:10:40', 39, NULL),
(224, 'commodi', '2017-07-05 16:15:14', 39, NULL),
(225, 'est', '2013-09-08 10:07:34', 40, NULL),
(226, 'temporibus', '2014-06-22 04:55:43', 40, NULL),
(227, 'officiis', '2018-01-27 19:53:35', 40, NULL),
(228, 'pariatur', '2011-05-15 20:59:53', 40, NULL),
(229, 'autem', '2016-05-24 01:36:00', 40, NULL),
(230, 'sit', '2010-04-15 02:38:12', 40, NULL),
(231, 'temporibus', '2016-02-27 13:02:24', 40, NULL),
(232, 'enim', '2011-07-18 16:18:39', 40, NULL),
(233, 'Le livre de la Jungle 2', '2016-03-03 00:00:00', 38, 'https://placehold.it/220/120');

-- --------------------------------------------------------

--
-- Structure de la table `ouvrage_auteur`
--

DROP TABLE IF EXISTS `ouvrage_auteur`;
CREATE TABLE IF NOT EXISTS `ouvrage_auteur` (
  `ouvrage_id` int(11) NOT NULL,
  `auteur_id` int(11) NOT NULL,
  PRIMARY KEY (`ouvrage_id`,`auteur_id`),
  KEY `IDX_3E39E6E815D884B5` (`ouvrage_id`),
  KEY `IDX_3E39E6E860BB6FE6` (`auteur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ouvrage_auteur`
--

INSERT INTO `ouvrage_auteur` (`ouvrage_id`, `auteur_id`) VALUES
(188, 100),
(189, 92),
(190, 93),
(191, 95),
(192, 95),
(193, 96),
(194, 97),
(195, 98),
(196, 99),
(197, 98),
(197, 99),
(198, 96),
(199, 98),
(200, 99),
(201, 99),
(202, 95),
(203, 96),
(204, 94),
(204, 100),
(205, 98),
(206, 95),
(206, 96),
(212, 93),
(213, 92),
(214, 92),
(215, 91),
(216, 91),
(217, 97),
(218, 94),
(219, 98),
(221, 91),
(222, 91),
(223, 92),
(224, 92),
(225, 94),
(226, 95),
(227, 95),
(228, 96),
(229, 97),
(230, 98),
(231, 92),
(231, 99),
(232, 99),
(233, 97);

-- --------------------------------------------------------

--
-- Structure de la table `ouvrage_mots_cles`
--

DROP TABLE IF EXISTS `ouvrage_mots_cles`;
CREATE TABLE IF NOT EXISTS `ouvrage_mots_cles` (
  `ouvrage_id` int(11) NOT NULL,
  `mots_cles_id` int(11) NOT NULL,
  PRIMARY KEY (`ouvrage_id`,`mots_cles_id`),
  KEY `IDX_22CE315815D884B5` (`ouvrage_id`),
  KEY `IDX_22CE3158C0BE80DB` (`mots_cles_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ouvrage_mots_cles`
--

INSERT INTO `ouvrage_mots_cles` (`ouvrage_id`, `mots_cles_id`) VALUES
(188, 106),
(188, 107),
(189, 107),
(189, 121),
(190, 108),
(190, 114),
(191, 109),
(192, 110),
(193, 111),
(194, 112),
(195, 113),
(196, 114),
(197, 115),
(198, 116),
(199, 117),
(201, 118),
(202, 119),
(203, 120),
(204, 121),
(205, 122),
(206, 123),
(207, 124),
(208, 125),
(209, 120),
(210, 111),
(216, 116),
(232, 125),
(233, 118);

-- --------------------------------------------------------

--
-- Structure de la table `rayon`
--

DROP TABLE IF EXISTS `rayon`;
CREATE TABLE IF NOT EXISTS `rayon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_rayon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `rayon`
--

INSERT INTO `rayon` (`id`, `nom_rayon`) VALUES
(36, 'voluptate'),
(37, 'corporis'),
(38, 'voluptas'),
(39, 'consequatur'),
(40, 'doloremque');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `exemplaire`
--
ALTER TABLE `exemplaire`
  ADD CONSTRAINT `FK_5EF83C921DB77787` FOREIGN KEY (`posseder_id`) REFERENCES `antenne` (`id`),
  ADD CONSTRAINT `FK_5EF83C92D793666D` FOREIGN KEY (`correspondre_id`) REFERENCES `ouvrage` (`id`);

--
-- Contraintes pour la table `exemplaire_adherents`
--
ALTER TABLE `exemplaire_adherents`
  ADD CONSTRAINT `FK_498F2AB915364D07` FOREIGN KEY (`adherents_id`) REFERENCES `adherents` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_498F2AB95843AA21` FOREIGN KEY (`exemplaire_id`) REFERENCES `exemplaire` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `ouvrage`
--
ALTER TABLE `ouvrage`
  ADD CONSTRAINT `FK_52A8CBD8E381DB8A` FOREIGN KEY (`ranger_id`) REFERENCES `rayon` (`id`);

--
-- Contraintes pour la table `ouvrage_auteur`
--
ALTER TABLE `ouvrage_auteur`
  ADD CONSTRAINT `FK_3E39E6E815D884B5` FOREIGN KEY (`ouvrage_id`) REFERENCES `ouvrage` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_3E39E6E860BB6FE6` FOREIGN KEY (`auteur_id`) REFERENCES `auteur` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `ouvrage_mots_cles`
--
ALTER TABLE `ouvrage_mots_cles`
  ADD CONSTRAINT `FK_22CE315815D884B5` FOREIGN KEY (`ouvrage_id`) REFERENCES `ouvrage` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_22CE3158C0BE80DB` FOREIGN KEY (`mots_cles_id`) REFERENCES `mots_cles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
