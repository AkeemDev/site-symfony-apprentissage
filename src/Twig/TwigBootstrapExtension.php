<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TwigBootstrapExtension extends AbstractExtension{

    public function getFilters()
    {
        return [new TwigFilter('badge', [$this, 'badgefilter'], ['is_safe' => ['html']]),
                new TwigFilter('button', [$this, 'buttonfilter'], ['is_safe' => ['html']])];
    }
    public function badgeFilter($content, array $options = []) : string 
    {
        $defaultOptions = [
            'color' => 'success'
        ];

        $options = array_merge($defaultOptions, $options);

        $color = $options['color'];


        return '<span class="badge badge-'. $color .' badge-pill ">' . $content . '</span>';
    }
    public function buttonFilter($content, array $options = []) : string 
    {
        $defaultOptions = [
            'color' => 'success'
        ];

        $options = array_merge($defaultOptions, $options);

        $color = $options['color'];


        return '<span class="btn btn-'. $color .' ">' . $content . '</span>';
    }
}