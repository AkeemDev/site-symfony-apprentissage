<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RayonRepository")
 */
class Rayon
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomRayon;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ouvrage", mappedBy="ranger")
     */
    private $ouvrages;

    public function __construct()
    {
        $this->ouvrages = new ArrayCollection();
    }
    
    public function __toString()
    {
        return $this->nomRayon;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomRayon(): ?string
    {
        return $this->nomRayon;
    }

    public function setNomRayon(?string $nomRayon): self
    {
        $this->nomRayon = $nomRayon;

        return $this;
    }

    /**
     * @return Collection|Ouvrage[]
     */
    public function getOuvrages(): Collection
    {
        return $this->ouvrages;
    }

    public function addOuvrage(Ouvrage $ouvrage): self
    {
        if (!$this->ouvrages->contains($ouvrage)) {
            $this->ouvrages[] = $ouvrage;
            $ouvrage->setRanger($this);
        }

        return $this;
    }

    public function removeOuvrage(Ouvrage $ouvrage): self
    {
        if ($this->ouvrages->contains($ouvrage)) {
            $this->ouvrages->removeElement($ouvrage);
            // set the owning side to null (unless already changed)
            if ($ouvrage->getRanger() === $this) {
                $ouvrage->setRanger(null);
            }
        }

        return $this;
    }
}
