<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExemplaireRepository")
 */
class Exemplaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $etat;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Adherents", inversedBy="exemplaires")
     */
    private $emprunter;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Antenne", inversedBy="exemplaires")
     */
    private $posseder;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ouvrage", inversedBy="exemplaires")
     */
    private $correspondre;

    public function __construct()
    {
        $this->emprunter = new ArrayCollection();
    }
    public function __toString()
    {
        return $this->etat;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return Collection|Adherents[]
     */
    public function getEmprunter(): Collection
    {
        return $this->emprunter;
    }

    public function addEmprunter(Adherents $emprunter): self
    {
        if (!$this->emprunter->contains($emprunter)) {
            $this->emprunter[] = $emprunter;
        }

        return $this;
    }

    public function removeEmprunter(Adherents $emprunter): self
    {
        if ($this->emprunter->contains($emprunter)) {
            $this->emprunter->removeElement($emprunter);
        }

        return $this;
    }

    public function getPosseder(): ?Antenne
    {
        return $this->posseder;
    }

    public function setPosseder(?Antenne $posseder): self
    {
        $this->posseder = $posseder;

        return $this;
    }

    public function getCorrespondre(): ?Ouvrage
    {
        return $this->correspondre;
    }

    public function setCorrespondre(?Ouvrage $correspondre): self
    {
        $this->correspondre = $correspondre;

        return $this;
    }
}
