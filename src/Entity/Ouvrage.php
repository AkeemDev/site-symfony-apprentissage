<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OuvrageRepository")
 */
class Ouvrage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateParution;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Exemplaire", mappedBy="correspondre")
     */
    private $exemplaires;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rayon", inversedBy="ouvrages")
     */
    private $ranger;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Auteur", inversedBy="ouvrages")
     */
    private $ecrire;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MotsCles", inversedBy="ouvrages")
     */
    private $definir;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    public function __construct()
    {
        $this->exemplaires = new ArrayCollection();
        $this->ecrire = new ArrayCollection();
        $this->definir = new ArrayCollection();
    }
    public function __toString()
    {
        return $this->titre;
    }
   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDateParution(): ?\DateTimeInterface
    {
        return $this->dateParution;
    }

    public function setDateParution(\DateTimeInterface $dateParution): self
    {
        $this->dateParution = $dateParution;

        return $this;
    }

    /**
     * @return Collection|Exemplaire[]
     */
    public function getExemplaires(): Collection
    {
        return $this->exemplaires;
    }

    public function addExemplaire(Exemplaire $exemplaire): self
    {
        if (!$this->exemplaires->contains($exemplaire)) {
            $this->exemplaires[] = $exemplaire;
            $exemplaire->setCorrespondre($this);
        }

        return $this;
    }

    public function removeExemplaire(Exemplaire $exemplaire): self
    {
        if ($this->exemplaires->contains($exemplaire)) {
            $this->exemplaires->removeElement($exemplaire);
            // set the owning side to null (unless already changed)
            if ($exemplaire->getCorrespondre() === $this) {
                $exemplaire->setCorrespondre(null);
            }
        }

        return $this;
    }

    public function getRanger(): ?Rayon
    {
        return $this->ranger;
    }

    public function setRanger(?Rayon $ranger): self
    {
        $this->ranger = $ranger;

        return $this;
    }

    /**
     * @return Collection|Auteur[]
     */
    public function getEcrire(): Collection
    {
        return $this->ecrire;
    }

    public function addEcrire(Auteur $ecrire): self
    {
        if (!$this->ecrire->contains($ecrire)) {
            $this->ecrire[] = $ecrire;
        }

        return $this;
    }

    public function removeEcrire(Auteur $ecrire): self
    {
        if ($this->ecrire->contains($ecrire)) {
            $this->ecrire->removeElement($ecrire);
        }

        return $this;
    }

    /**
     * @return Collection|MotsCles[]
     */
    public function getDefinir(): Collection
    {
        return $this->definir;
    }

    public function addDefinir(MotsCles $definir): self
    {
        if (!$this->definir->contains($definir)) {
            $this->definir[] = $definir;
        }

        return $this;
    }

    public function removeDefinir(MotsCles $definir): self
    {
        if ($this->definir->contains($definir)) {
            $this->definir->removeElement($definir);
        }

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
