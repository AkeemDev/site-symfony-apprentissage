<?php

namespace App\DataFixtures;

use App\Entity\Adherents;
use App\Entity\Antenne;
use App\Entity\Auteur;
use App\Entity\Exemplaire;
use App\Entity\MotsCles;
use App\Entity\Ouvrage;
use App\Entity\Rayon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Faker extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
      

        for( $i = 0; $i<75 ; $i++)
        {
            $auteur = new Auteur();
            $auteur->setNom($faker->lastname)
            ->setPrenom($faker->firstName)
            ->setNationalite($faker->country)
            ->setDateNaissance($faker->dateTimeThisDecade($max = 'now', $timezone = null));
            $manager->persist($auteur);
        }
        
        for( $i = 0; $i<6 ; $i++)
        {
            $rayon = new Rayon();
            $rayon->setNomRayon($faker->word(1,2));
            $manager->persist($rayon);
            for( $j = 0; $j<mt_rand(40,60) ; $j++)
            {
                $ouvrage = new Ouvrage();
                $mot1 = $faker->word;
                $mot2 = $faker->word;
                $titre = $mot1 ." ". $mot2; 
                $slug = $mot1 ."-". $mot2; 
                $ouvrage->setDateParution($faker->dateTimeThisDecade($faker->dateTimeThisYear(), $timezone = null));
                $ouvrage->setTitre($titre);
                $ouvrage->setSlug($slug);
                $ouvrage->setDescription($faker->sentence(mt_rand(10,20)));
                $ouvrage->setContent($faker->paragraph(mt_rand(25,70)));
                $ouvrage->setRanger($rayon);
                $ouvrage->setImage('http://placehold.it/80x110');
                // $ouvrage->addEcrire();
    
                $manager->persist($ouvrage);
            }
        }
        for( $i = 0; $i<40 ; $i++)
        {
            $mc = new MotsCles();
            $mc->setMotsCles($faker->word(1,2));
            $manager->persist($mc);
        }
        for( $i = 0; $i<13 ; $i++)
        {
            $antenne = new Antenne();
            $antenne->setVille($faker->city);
            $antenne->setRegion($faker->word);

            $manager->persist($antenne);
        }
        for( $i = 0; $i<80 ; $i++)
        {
            $adh = new Adherents();
            $adh->setPseudo($faker->firstName);
            $adh->setPrenom($faker->firstName);
            $adh->setEmail($faker->email);
            $manager->persist($adh);
        }
        for( $i = 0; $i<1 ; $i++)
        {
            $exemplaire = new Exemplaire();
            $exemplaire->setEtat('neuf');
            $exemplaire->setPosseder($antenne);
            $exemplaire->setCorrespondre($ouvrage);

            $manager->persist($exemplaire);
        }



        $manager->flush();
    }
}
