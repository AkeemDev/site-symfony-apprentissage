<?php

namespace App\Controller;

use App\Entity\Adherents;
use App\Entity\Exemplaire;
use App\Repository\AdherentsRepository;
use App\Repository\AntenneRepository;
use App\Repository\AuteurRepository;
use App\Repository\ExemplaireRepository;
use App\Repository\MotsClesRepository;
use App\Repository\OuvrageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AjoutDonneesController extends AbstractController
{
    /**
     * @Route("/essai/ajout/ouf", name="essai")
     */
    public function OuvrageAuteur(AuteurRepository $auteurRepository, OuvrageRepository $ouvrageRepository, EntityManagerInterface $manager)
    {
        $auteurs = $auteurRepository->findAll(); 
        $ouvrages = $ouvrageRepository->findAll();
    
        foreach ($ouvrages as $ouvrage )
        {
            $numAuteur = array_rand($auteurs, $num = 1);
            $ouvrage->addEcrire($auteurs[$numAuteur]);
            $manager->persist( $ouvrage);
        }
       

        for ($i=0; $i < 40 ; $i++) { 

            $numAuteur = array_rand($auteurs, $num = 1);
            $numOuvrage = array_rand($ouvrages, $num = 1);
            $ouvrage->addEcrire($auteurs[$numAuteur]);

            $ouvrage = $ouvrages[$numOuvrage];
            $ouvrage->addEcrire($auteurs[$numAuteur]);
            $manager->persist($ouvrage);
        }
        $manager->flush();

        return $this->render('ajout_donnees/index.html.twig', [
            'controller_name' => 'AjoutDonneesController',
        ]);
    }
    /**
     * @Route("/essai2/ouf/pasbien", name="essai2")
     */
    public function OuvrageMotsClefs(OuvrageRepository $ouvrageRepository, MotsClesRepository $motsClesRepository, EntityManagerInterface $manager){

        $ouvrages = $ouvrageRepository->findAll();        
        $motsCles = $motsClesRepository->findAll();

        for ($i=0; $i < 100; $i++) { 
            $randomMotsCles = array_rand($motsCles, $num = 1);
            $randomOuvrage = array_rand($ouvrages, $num = 1);
            $ouvrage = $ouvrages[$randomOuvrage]->addDefinir($motsCles[$randomMotsCles]);
            
            $manager->persist($ouvrage);
        }

        // foreach ($ouvrages as  $ouvrage) {
        //     $randomMotsCles = array_rand($motsCles, $num = 1);
        //     $ouvrage->addDefinir($motsCles[$randomMotsCles]);

        //     $manager->persist($ouvrage);
        // }
        $manager->flush();
        
        return $this->render('ajout_donnees/index.html.twig', [
            'controller_name' => 'AjoutDonneesController',
        ]);
    }

    /**
     * @Route("/essai2/ourge/dgdfg", name="essai2dfg")
     */
    public function ExemplaireAdherent( OuvrageRepository $ouvrageRepository, AntenneRepository $antenneRepository, EntityManagerInterface $manager){

        $ouvrages = $ouvrageRepository->findAll();        
        $antennes = $antenneRepository->findAll();
        $etat = ['neuf','bon etat','moyen', 'abime'];
       


        for ($i = 0; $i < 1200; $i++) {

            $numOuvrage = array_rand($ouvrages, 1);
            $numAntenne = array_rand($antennes, 1);
            $numEtat = array_rand($etat, 1);
            
            $exemplaire = new Exemplaire();
            $exemplaire->setEtat($etat[$numEtat]);
            $exemplaire->setPosseder($antennes[$numAntenne]);
            $exemplaire->setCorrespondre($ouvrages[$numOuvrage]);
           

            $manager->persist($exemplaire);
        }
        $manager->flush();
        
        return $this->render('ajout_donnees/index.html.twig', [
            'controller_name' => 'AjoutDonneesController',
        ]);
        }
     /**
     * @Route("/livreDOr", name="livreDOr")
     */
    public function livreDor (OuvrageRepository $repo)
    {
        $livreOr = $repo->livreOr();
        return $this->render('ajout_donnees/livreOr.html.twig', [
            'controller_name' => 'AjoutDonneesController',
            'livreOr' => $livreOr
        ]);

    }
}
