<?php

namespace App\Controller;

use App\Entity\Auteur;
use App\Entity\MotsCles;
use App\Repository\AuteurRepository;
use App\Repository\MotsClesRepository;
use App\Repository\OuvrageRepository;
use PhpParser\Node\Stmt\Foreach_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Length;

class AuteurController extends AbstractController
{
    /**
     * @Route("/auteur", name="show_auteurs")
     */
    public function index()
    {
        return $this->render('auteur/index.html.twig', [
            'controller_name' => 'AuteurController',
        ]);
    }
    /**
     * @Route("/auteur/{id}", name="show_auteur")
     */
    public function showAuteur(Auteur $auteur, AuteurRepository $auteurRepo )
    {
        $auteurId = $auteur->getId();
        
        $ouvragesAuteur = $auteurRepo->ouvragesAuteur($auteur);
 

        return $this->render('auteur/showAuteur.html.twig', [
            'ouvrages' => $ouvragesAuteur,
            'auteur' => $auteur,
        ]);
    }
}
