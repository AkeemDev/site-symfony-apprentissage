<?php

namespace App\Controller;

use App\Entity\Ouvrage;
use App\Entity\OuvragesRayon;
use App\Form\OuvragesRayonType;
use App\Form\OuvrageType;
use App\Repository\AuteurRepository;
use App\Repository\MotsClesRepository;
use App\Repository\OuvrageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OuvrageController extends AbstractController
{


    /**
     * @Route("/", name="accueil")
     */
    public function accueil()
    {
        return $this->render('ajout_donnees/index.html.twig');
    }



    /**
     * @Route("/ouvrages", name="show_ouvrages")
     */
    public function requete(
        OuvrageRepository $ouvrageRepository,
        Request $request,
        PaginatorInterface $paginator
    ) {
        $detailsOuvrages = $ouvrageRepository->essaiReq();
        $ouvragesPaginees = $paginator->paginate($detailsOuvrages, $request->query->getInt('page', 1), 10);
        dump($detailsOuvrages);

        return $this->render('ouvrage/ouvrages.html.twig', [
            'ouvrages' => $ouvragesPaginees,
            'ouvragesTotal' => $detailsOuvrages,
        ]);
    }
    /**
     * @Route("/ouvrages/{id}/edit", name="edit_ouvrages")
     */
    public function addLiaisonMotsCles(Ouvrage $ouvrage, EntityManagerInterface $manager, Request $request)
    {


        $form = $this->createForm(OuvrageType::class, $ouvrage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($ouvrage);
            $manager->flush();
            return $this->redirectToRoute('show_ouvrages');
        }


        return $this->render('ouvrage/add.html.twig', [
            'ouvrage' => $form->createView()
        ]);
    }
    /**
     * @Route("/ouvrage/{slug}", name="showOuvrage")
     */
    public function showOuvrage(Ouvrage $ouvrage, OuvrageRepository $ouvrageRepository, MotsClesRepository $motsClesRepository)
    {
        $DetailOuvrage = $ouvrageRepository->detailsOuvrage($ouvrage);
        $motsCles = $motsClesRepository->motsClesAuteur($ouvrage);
        $DetailOuvrage = $DetailOuvrage[0];
        $auteurs = $ouvrageRepository->OuvrageAuteurs($ouvrage);
        dump($DetailOuvrage);

        return $this->render('ouvrage/ouvrage.html.twig', [
            'detail' => $DetailOuvrage,
            'auteurs' => $auteurs,
            'mcs' => $motsCles
        ]);
    }
    /**
     * @Route("/paginator", name="paginator")
     */
    public function paginator(
        OuvrageRepository $ouvrageRepository,
        PaginatorInterface $paginator,
        Request $request
    )
    {
        $ouvrages = $ouvrageRepository->FindAll();
        $ouvragesPaginees = $paginator->paginate(
            $ouvrages,
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            16 // Nombre de résultats par page);
        );
        return $this->render('ouvrage/paginate.html.twig', [
            'ouvrages' => $ouvragesPaginees,
        ]);
    }
}
