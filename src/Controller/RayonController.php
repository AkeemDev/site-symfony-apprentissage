<?php

namespace App\Controller;

use App\Entity\Auteur;
use App\Entity\Rayon;
use App\Entity\SearchRayon;
use App\Form\RayonType;
use App\Form\SearchRayonType;
use App\Repository\RayonRepository;
use PhpParser\Node\Stmt\Foreach_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RayonController extends AbstractController
{
    /**
     * @Route("/rayons", name="rayons")
     */
    public function index(RayonRepository $rayonRepository)
    {
        $rayons = $rayonRepository->findAll();
        dump($rayons);
        return $this->render('rayon/index.html.twig', [
            'rayons' => $rayons
        ]);
    }
     /**
     * @Route("/rayon/{id}", name="show_rayon")
     */
    public function showRayon(Rayon $rayon, RayonRepository $repo)
    {
        $rayonId = $rayon->getId();
        $ouvragesRayon = $repo->ouvragesRayon($rayonId);
        dump($ouvragesRayon);
        return $this->render('rayon/rayon.html.twig', [
            'ouvragesRayon' => $ouvragesRayon
        ]);
    }

     /**
     * @Route("/search", name="search")
     */
    public function searchOuvragesRayon(Request $request, RayonRepository $repo){

        $rayon = new Rayon;
        $form = $this->createForm(RayonType::class, $rayon);
        $form->handleRequest($request);
        $allRayons = $repo->findAll();



        if($form->isSubmitted() && $form->isValid())
        {
            $ouvragesRayon = $repo->searchOuvragesRayon($rayon);
            $id = $ouvragesRayon[0]['id'];
            $donnees = $repo->OuvragesRayon($id);
            dump($id);
            dump($donnees);

            // return $this->render('/rayon/rayon.html.twig',[
                return $this->redirectToRoute('show_rayon', [               
                'id' => $id,
                'ouvragesRayon' => $donnees
            ]);

        }
        return $this->render('rayon/searchRayon.html.twig', [
            'form' => $form->createView(),
            'rayons' => $allRayons
        ]);
        
    }
}
