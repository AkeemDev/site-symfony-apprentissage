<?php

namespace App\Form;

use App\Entity\Auteur;
use App\Entity\MotsCles;
use App\Entity\Ouvrage;
use App\Entity\Rayon;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OuvrageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('dateParution', DateType::class)
            ->add('ranger',EntityType::class, 
            [
                'class' => Rayon::class,
                'label' => 'Rayon'
            ])
            ->add('ecrire', CollectionType::class,[
                'entry_type' => AuteurType::class,
                // these options are passed to each "email" type
                'entry_options' => [
                    'attr' => ['class' => 'essai'],
                ]])
            ->add('definir', CollectionType::class,[
                'entry_type' => MotsClesType::class,
                'entry_options' => [
                'attr' =>  ['class' => 'essai'],
                ]])
            ->add('Editer', SubmitType::class );
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ouvrage::class,
        ]);
    }
}
