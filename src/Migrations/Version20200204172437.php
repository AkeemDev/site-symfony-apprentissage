<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200204172437 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE exemplaire_adherents (exemplaire_id INT NOT NULL, adherents_id INT NOT NULL, INDEX IDX_498F2AB95843AA21 (exemplaire_id), INDEX IDX_498F2AB915364D07 (adherents_id), PRIMARY KEY(exemplaire_id, adherents_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ouvrage_auteur (ouvrage_id INT NOT NULL, auteur_id INT NOT NULL, INDEX IDX_3E39E6E815D884B5 (ouvrage_id), INDEX IDX_3E39E6E860BB6FE6 (auteur_id), PRIMARY KEY(ouvrage_id, auteur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ouvrage_mots_cles (ouvrage_id INT NOT NULL, mots_cles_id INT NOT NULL, INDEX IDX_22CE315815D884B5 (ouvrage_id), INDEX IDX_22CE3158C0BE80DB (mots_cles_id), PRIMARY KEY(ouvrage_id, mots_cles_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exemplaire_adherents ADD CONSTRAINT FK_498F2AB95843AA21 FOREIGN KEY (exemplaire_id) REFERENCES exemplaire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE exemplaire_adherents ADD CONSTRAINT FK_498F2AB915364D07 FOREIGN KEY (adherents_id) REFERENCES adherents (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ouvrage_auteur ADD CONSTRAINT FK_3E39E6E815D884B5 FOREIGN KEY (ouvrage_id) REFERENCES ouvrage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ouvrage_auteur ADD CONSTRAINT FK_3E39E6E860BB6FE6 FOREIGN KEY (auteur_id) REFERENCES auteur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ouvrage_mots_cles ADD CONSTRAINT FK_22CE315815D884B5 FOREIGN KEY (ouvrage_id) REFERENCES ouvrage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ouvrage_mots_cles ADD CONSTRAINT FK_22CE3158C0BE80DB FOREIGN KEY (mots_cles_id) REFERENCES mots_cles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE exemplaire ADD posseder_id INT DEFAULT NULL, ADD correspondre_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exemplaire ADD CONSTRAINT FK_5EF83C921DB77787 FOREIGN KEY (posseder_id) REFERENCES antenne (id)');
        $this->addSql('ALTER TABLE exemplaire ADD CONSTRAINT FK_5EF83C92D793666D FOREIGN KEY (correspondre_id) REFERENCES ouvrage (id)');
        $this->addSql('CREATE INDEX IDX_5EF83C921DB77787 ON exemplaire (posseder_id)');
        $this->addSql('CREATE INDEX IDX_5EF83C92D793666D ON exemplaire (correspondre_id)');
        $this->addSql('ALTER TABLE ouvrage ADD ranger_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ouvrage ADD CONSTRAINT FK_52A8CBD8E381DB8A FOREIGN KEY (ranger_id) REFERENCES rayon (id)');
        $this->addSql('CREATE INDEX IDX_52A8CBD8E381DB8A ON ouvrage (ranger_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE exemplaire_adherents');
        $this->addSql('DROP TABLE ouvrage_auteur');
        $this->addSql('DROP TABLE ouvrage_mots_cles');
        $this->addSql('ALTER TABLE exemplaire DROP FOREIGN KEY FK_5EF83C921DB77787');
        $this->addSql('ALTER TABLE exemplaire DROP FOREIGN KEY FK_5EF83C92D793666D');
        $this->addSql('DROP INDEX IDX_5EF83C921DB77787 ON exemplaire');
        $this->addSql('DROP INDEX IDX_5EF83C92D793666D ON exemplaire');
        $this->addSql('ALTER TABLE exemplaire DROP posseder_id, DROP correspondre_id');
        $this->addSql('ALTER TABLE ouvrage DROP FOREIGN KEY FK_52A8CBD8E381DB8A');
        $this->addSql('DROP INDEX IDX_52A8CBD8E381DB8A ON ouvrage');
        $this->addSql('ALTER TABLE ouvrage DROP ranger_id');
    }
}
