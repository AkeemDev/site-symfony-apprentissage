<?php

namespace App\Repository;

use App\Entity\MotsCles;
use App\Entity\Ouvrage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MotsCles|null find($id, $lockMode = null, $lockVersion = null)
 * @method MotsCles|null findOneBy(array $criteria, array $orderBy = null)
 * @method MotsCles[]    findAll()
 * @method MotsCles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MotsClesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MotsCles::class);
    }

    public function motsClesAuteur(Ouvrage $ouvrage)
    {
        $rawSql = "SELECT * from ouvrage
        right join ouvrage_mots_cles
        on ouvrage.id = ouvrage_mots_cles.ouvrage_id
        inner join mots_cles
        on ouvrage_mots_cles.mots_cles_id = mots_cles.id
        where ouvrage_mots_cles.ouvrage_id = :ouvrage_id
        ";
    
        $ouvrageId = $ouvrage->getId();
        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->execute(['ouvrage_id' => $ouvrageId]);
    
        return $stmt->fetchAll();
    }
    public function motscles()
    {
        $rawSql = "SELECT * from ouvrage
        inner join ouvrage_mots_cles
        on ouvrage.id = ouvrage_mots_cles.ouvrage_id
        inner join mots_cles
        on ouvrage_mots_cles.mots_cles_id = mots_cles.id
        ";
    
        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->execute([]);
    
        return $stmt->fetchAll();
    }
    // public function motsClesOuvrage($ouvrage)
    // {
    //     $rawSql = "SELECT * from ouvrage
    //     inner join 
    //     "
    // }
    // /**
    //  * @return MotsCles[] Returns an array of MotsCles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MotsCles
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
