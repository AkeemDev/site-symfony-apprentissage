<?php

namespace App\Repository;

use App\Entity\Auteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Auteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Auteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Auteur[]    findAll()
 * @method Auteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Auteur::class);
    }
    // return auteur, ses ouvrages, les mots cles de l'ouvrage, le rayon d'un ouvrage
    public function ouvragesAuteur(Auteur $auteur)
    {
        $rawSql = "SELECT * from ouvrage
        inner join ouvrage_auteur
        on ouvrage.id = ouvrage_auteur.ouvrage_id
        inner join auteur
        on ouvrage_auteur.auteur_id = auteur.id

        inner join rayon
        on ouvrage.ranger_id = rayon.id

        where ouvrage_auteur.auteur_id = :auteur
        order by ouvrage.id desc
        ";
    
        $auteurId = $auteur->getId();
        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->execute(['auteur' => $auteurId]);
    
        return $stmt->fetchAll();
    }
    
    // /**
    //  * @return Auteur[] Returns an array of Auteur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Auteur
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
