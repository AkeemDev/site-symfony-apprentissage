<?php

namespace App\Repository;

use App\Entity\Auteur;
use App\Entity\Ouvrage;
use App\Entity\OuvragesRayon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Ouvrage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ouvrage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ouvrage[]    findAll()
 * @method Ouvrage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OuvrageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ouvrage::class);
    }

    public function essaiReq()
    {
        $rawSql = "SELECT * from ouvrage
        inner join ouvrage_auteur
        on ouvrage.id = ouvrage_auteur.ouvrage_id
        inner join auteur
        on ouvrage_auteur.auteur_id = auteur.id

        inner join rayon
        on ouvrage.ranger_id = rayon.id

        inner join ouvrage_mots_cles
        on ouvrage.id = ouvrage_mots_cles.ouvrage_id
        inner join mots_cles
        on ouvrage_mots_cles.mots_cles_id = mots_cles.id
      
        group by titre
        order by ouvrage.id asc
        ";
    
        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->execute([]);
    
        return $stmt->fetchAll();
    }
    public function OuvrageAuteurs(Ouvrage $ouvrage){
        $rawSql = "SELECT * from ouvrage
        inner join ouvrage_auteur
        on ouvrage_auteur.ouvrage_id = ouvrage.id
        inner join auteur
        on auteur.id = ouvrage_auteur.auteur_id
        where ouvrage.id = :ouvrage
        ";

          $id = $ouvrage -> getId();
          $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
          $stmt->execute(['ouvrage' => $id]);
          return $stmt->fetchAll();
    }
    public function detailsOuvrage(Ouvrage $ouvrage){
        $rawSql = "SELECT * from ouvrage
            inner join ouvrage_mots_cles
            on ouvrage.id = ouvrage_mots_cles.ouvrage_id
            inner join mots_cles
            on ouvrage_mots_cles.mots_cles_id = mots_cles.id
            inner join rayon
            on ouvrage.ranger_ID = rayon.id
            inner join ouvrage_auteur
            on ouvrage.id = ouvrage_auteur.ouvrage_id
            where ouvrage.id = :id
        ";
// 
        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->execute(['id' => $ouvrage->getId()]);
    
        return $stmt->fetchAll();
    }
   
    public function FindAllOuvragePaginate()
    {
        $rawSql = "SELECT * from ouvrage";
        
        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->execute([]);
        return $stmt->fetchAll();

    }
    public function livreOr(){    
            $rawSql = " SELECT ouvrage.titre from ouvrage
            inner join rayon
            on rayon.id = ouvrage.ranger_id
            
            inner join ouvrage_mots_cles
            on ouvrage_mots_cles.ouvrage_id = ouvrage.id
            inner join mots_cles 
            on mots_cles.id = ouvrage_mots_cles.mots_cles_id
            
            inner join ouvrage_auteur
            on ouvrage.id = ouvrage_auteur.ouvrage_id
            inner join auteur
            on auteur.id = ouvrage_auteur.auteur_id
            
            where ouvrage.id = 209 and auteur.nom = 'bonaparte' and mots_cles = 'fugit'
            ";
    
            $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
            $stmt->execute([]);
        
            return $stmt->fetchAll();
        }

        public function filtration(OuvragesRayon $rayon){    
            $rawSql = " SELECT * from ouvrage
            inner join rayon
            on rayon.id = ouvrage.ranger_id
            where rayon.nom_rayon = :rayon 
            ";
    
            $rayon->getRayon();
            $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
            $stmt->execute(['rayon' => $rayon]);
        
            return $stmt->fetchAll();
        }
        
       
    
    // /**
    //  * @return Ouvrage[] Returns an array of Ouvrage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ouvrage
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
